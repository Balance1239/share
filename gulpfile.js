'use strict';

var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglifyjs');

function scripts() {
    return gulp.src('./app/sass/*.scss')  
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./app/css'))    
}

function libsjs() {
    return gulp.src([
        './app/libs/jquery/dist/jquery.min.js',
        './app/libs/magnific-popup/dist/jquery.magnific-popup.min.js',
    ])
    .pipe(concat('libs.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./app/js'));
}

function reload(done) {
  browserSync.reload();
  done();
} 

function serve(done) {
    browserSync.init({
    server: {
      baseDir: './app'
    },
    notify: false
  });
  done();
}

const watch = () => gulp.watch([
                        './app/sass/**/*.scss',
                        './app/*.html',
                        './app/js/*.js'], 
                    gulp.series(scripts, reload));

const dev = gulp.series(scripts, libsjs, serve, watch);
exports.default = dev




//--------------------it works well too---------------------------------------
// var gulp        = require('gulp');
// var browserSync = require('browser-sync').create();
// var sass        = require('gulp-sass');



// sass.compiler = require('node-sass');

// gulp.task('sass', function() {
//     return gulp.src('./app/sass/*.scss')
//             .pipe(sass().on('error', sass.logError))
//             .pipe(gulp.dest('./app/css'))
//             .pipe(browserSync.reload({
//                 stream: true
//             }));
// });

// gulp.task('browser-sync', function() {
//     browserSync.init({
//         server: {
//             baseDir: "./app"
//         },
//         notify: false
//     }); 
//     gulp.watch('./app/sass/*.scss', gulp.series('sass'));  
// });
//-----------------------------------------------------------------------------
